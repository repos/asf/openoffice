<?xml version="1.0" encoding="UTF-8"?>

<!--***********************************************************
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 ***********************************************************-->

<helpdocument version="1.0">
<meta>
<topic id="textsimpress0101180001xml" indexer="include" status="PUBLISH">
<title id="tit" xml-lang="en-US">Page</title>
<filename>/text/simpress/01/01180001.xhp</filename>
</topic>
</meta>
<body>
<section id="seite">
<bookmark xml-lang="en-US" branch="index" id="bm_id3154011">
<bookmark_value>slides; formatting</bookmark_value>
<bookmark_value>formatting;slides</bookmark_value>
</bookmark>
<paragraph role="heading" id="hd_id3154011" xml-lang="en-US" level="1" l10n="U"><link href="text/simpress/01/01180001.xhp" name="Page">Page</link></paragraph>
<paragraph role="paragraph" id="par_id3153416" xml-lang="en-US" l10n="U">Sets page orientation, page margins, background and other layout options.</paragraph>
</section>
<section id="howtoget">
<embed href="text/simpress/00/00000405.xhp#frtites"/>
</section>
<paragraph role="heading" id="hd_id3155445" xml-lang="en-US" level="2" l10n="U">Paper format</paragraph>
<paragraph role="heading" id="hd_id3154703" xml-lang="en-US" level="3" l10n="U">Format</paragraph>
<paragraph role="paragraph" id="par_id3150299" xml-lang="en-US" l10n="U">Select a paper format supported by your printer. You can also create a custom page size by selecting <emph>User</emph> and entering the size dimensions in the <emph>Width</emph> and <emph>Height</emph> boxes.</paragraph>
<paragraph role="heading" id="hd_id3154659" xml-lang="en-US" level="3" l10n="U">Width</paragraph>
<paragraph role="paragraph" id="par_id3152992" xml-lang="en-US" l10n="U">Shows the width of the paper format you selected in the <emph>Format</emph> box. If you selected the <emph>User</emph> format, enter a value for the width of the page.</paragraph>
<paragraph role="heading" id="hd_id3153816" xml-lang="en-US" level="3" l10n="U">Height</paragraph>
<paragraph role="paragraph" id="par_id3149945" xml-lang="en-US" l10n="U">Shows the height of the paper format you selected in the <emph>Format</emph> box. If you selected the <emph>User</emph> format, enter a value for the height of the page.</paragraph>
<paragraph role="heading" id="hd_id3159207" xml-lang="en-US" level="3" l10n="U">Portrait</paragraph>
<paragraph role="paragraph" id="par_id3153250" xml-lang="en-US" l10n="U">Page orientation is vertical.</paragraph>
<paragraph role="heading" id="hd_id3154766" xml-lang="en-US" level="3" l10n="U">Landscape</paragraph>
<paragraph role="paragraph" id="par_id3153812" xml-lang="en-US" l10n="U">Page orientation is horizontal.</paragraph>
<paragraph role="heading" id="hd_id3153075" xml-lang="en-US" level="3" l10n="U">Paper tray</paragraph>
<paragraph role="paragraph" id="par_id3145115" xml-lang="en-US" l10n="U">Select the paper source for your printer.</paragraph>
<paragraph role="tip" id="par_id3150652" xml-lang="en-US" l10n="U">If your document uses more than one paper format, you can select a different tray for each format.</paragraph>
<embed href="text/shared/00/00000001.xhp#vorschau"/>
<paragraph role="heading" id="hd_id3150746" xml-lang="en-US" level="2" l10n="U">Margins</paragraph>
<paragraph role="paragraph" id="par_id3153037" xml-lang="en-US" l10n="U">Specify the distance between the edge of a printed page and the printable area.</paragraph>
<paragraph role="heading" id="hd_id3145591" xml-lang="en-US" level="3" l10n="U">Left</paragraph>
<paragraph role="paragraph" id="par_id3154561" xml-lang="en-US" l10n="U">Enter the distance between the left edge of the page and the data. You can see the result in the preview.</paragraph>
<paragraph role="heading" id="hd_id3153084" xml-lang="en-US" level="3" l10n="U">Right</paragraph>
<paragraph role="paragraph" id="par_id3153001" xml-lang="en-US" l10n="U">Enter the distance between the right edge of the page and the data. You can see the result in the preview.</paragraph>
<paragraph role="heading" id="hd_id3153565" xml-lang="en-US" level="3" l10n="U">Top</paragraph>
<paragraph role="paragraph" id="par_id3145167" xml-lang="en-US" l10n="U">Enter the distance between the top edge of the page and the data. You can see the result in the preview.</paragraph>
<paragraph role="heading" id="hd_id3150335" xml-lang="en-US" level="3" l10n="U">Bottom</paragraph>
<paragraph role="paragraph" id="par_id3153736" xml-lang="en-US" l10n="U">Enter the distance between the bottom edge of the page and the data. You can see the result in the preview.</paragraph>
<paragraph role="heading" id="hd_id3150018" xml-lang="en-US" level="3" l10n="U">Format</paragraph>
<paragraph role="paragraph" id="par_id3149877" xml-lang="en-US" l10n="U">Specify the format for page numbering.</paragraph>
<paragraph role="heading" id="hd_id3155439" xml-lang="en-US" level="3" l10n="U">Fit object to paper format</paragraph>
<paragraph role="paragraph" id="par_id3153042" xml-lang="en-US" l10n="U">Reduces the scale of objects and the size of the font on the page so that they print on the selected paper format.</paragraph>
</body>
</helpdocument>
